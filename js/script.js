// Знайти всі параграфи на сторінці та встановити колір фону #ff0000

let listTagName = document.querySelectorAll("p");

listTagName.forEach((element) => {
  element.style.background = "#ff0000";
});

// Знайти елемент із id="optionsList". Вивести у консоль.

let optionsList = document.getElementById("optionsList");
console.log(optionsList);

// Знайти батьківський елемент та вивести в консоль

let fatherElement = document.querySelector("#optionsList");
console.log(fatherElement.parentElement);

// Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

console.log(optionsList.childNodes);

console.log(optionsList.childNodes[0].nodeName);
console.log(optionsList.childNodes[0].nodeType);

// Встановіть в якості контента елемента з класом testParagraph наступний параграф -

// This is a paragraph

let testParagraph = document.getElementById("testParagraph");

let p = document.createElement("p");
p.innerText += "This is a paragraph";

testParagraph.appendChild(p);

// Отримати елементи , вкладені в елемент із класом main-header і вивести їх у консоль.
// Кожному з елементів присвоїти новий клас nav-item.

const mainHeader = document.getElementsByClassName("main-header");

Array.from(mainHeader).forEach((element) => {
  console.log(element);
  element.classList.add('nav-item');
});

//  Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
let sectionTitle = document.querySelectorAll(".section-title");

sectionTitle.forEach(function (elem) {
  elem.classList.remove("section-title");
});
